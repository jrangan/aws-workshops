Cheatsheet of Useful Cloudformation CLI Commands
================================================

- Created/Update Stack without explicitly telling it to update

```js
aws cloudformation deploy
--stack-name my-statically-hosted-website-stack
--template-file ./my-statically-hosted-website.template.json
--parameter-overrides CustomTag=BestTagEvar,AnotherTag=tagvalue
--profile pf-sandbox-developer;
```

- View Stacks (Describe Stack)

```js
aws cloudformation describe-stacks
--stack-name my-statically-hosted-website-stack
--profile pf-sandbox-developer
```

- View Stack Resources

```js
aws cloudformation describe-stack-resources
--stack-name my-statically-hosted-website-stack
--profile pf-sandbox-developer
```

- Delete a Stack

```js
aws cloudformation delete-stack
--stack-name my-statically-hosted-website-stack
--profile pf-sandbox-developer
```

Key commands (aws cloudformation)

```js
deploy                          //Creates and Deploys Template
deploy --no-execute-changeset   //
describe-change-set             //Shows changes to be made
execute-change-set              //Deploys the change-set
```

- Create change-set (To check what our stack deployment would look like)

```js
// this is the command with arguments
aws cloudformation deploy
--stack-name my-statically-hosted-website-stack              //Create a Stack Name
--template-file ./my-statically-hosted-website.template.json //Template File
--profile pf-sandbox-developer                               //AWS Profile
--no-execute-changeset;                                      //Do not deploy
```

- Execute the changeset (Will create the stack and its resources)

```js
aws cloudformation execute-change-set                      //Carry out the executed changes
--change-set-name arn:aws:cloudformation:somemore:arn
--profile pf-sandbox-developer
```