#!/bin/bash
#set -e

# Bash Script deploys or destroys an s3 bucket and cloud front distribution
# Note: AWS Token must be refreshed to use this script

uniqueID=$(uuidgen)
bucketID="$2-$uniqueID"

# Create s3 bucket
makeBucket() {
    #Remove line
    #echo "LOG : bucket : $bucketID" >> ~/scripts/aws-websites.log
    aws s3 mb s3://$bucketID --profile $2
}

makePolicyFile() {
    bucketPolicyFile=$(mktemp /tmp/bucket-policy.XXXXXX.json)

    # Write policy to file
    cat <<EOF >$bucketPolicyFile
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::$1/*"
            ]
        }
    ]
}
EOF

    # Add Bucket Policy
    aws s3api put-bucket-policy --bucket $1 --profile $2 --policy file://$bucketPolicyFile

    rm $bucketPolicyFile

    echo __BUCKET POLICY__
    # Output Policy
    aws s3api get-bucket-policy --bucket $1 --profile $2 --output text
}
# Setup s3 bucket
setupBucket() {
    # Enable static web hosting - use bucket to host a website
    aws s3 website s3://$1 --index-document index.html --profile $2
    echo "Static Web Hosting: Enabled"

    #Delete public access block - so website is publically accessable
    aws s3api delete-public-access-block --bucket $1 --profile $2
    echo "Public Access: Enabled"

    # View public access settings
    # aws s3api get-public-access-block --bucket $bucketID --profile $2

    # Apply Bucket Policy
    makePolicyFile "$1" "$2"
}

# Deploy Website
deploy() {

    # Production Build
    npm run build
    cd build

    # Create s3 bucket
    makeBucket "$1" "$2"
    setupBucket "$1" "$2"

    # Deploy Production Bundle to s3 Bucket
    aws s3 sync . s3://$1 --profile $2

    # Verify Sync
    aws s3 ls s3://$1 --recursive --human-readable --summarize --profile $2

    # Create Cloudfront Distribution
    aws cloudfront create-distribution --origin-domain-name $1.s3-website-ap-southeast-2.amazonaws.com --default-root-object index.html --profile $2
}

# Destroy Website
destroy() {
    echo REMOVING BUCKET
    aws s3 rb --force s3://$2 --profile $3
}

if [ $1 = deploy ]; then
    # Pass bucketID and Profile Name
    deploy "$bucketID" "$3"
elif [ $1 = destroy ]; then
    # Pass Bucket Name and Profile Name
    destroy "$2" "$3"
else
    echo INVALID: Specify Deploy or Destroy as the first argument
fi
