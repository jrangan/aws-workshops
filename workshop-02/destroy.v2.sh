#!/bin/bash

# How to Run
# ./destroy <SSO Profile> <Bucket Name> <Cloud Distribution Id>
set -e -x
export AWS_PROFILE="$1"

disableDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"
    local DISTRIBUTION_CONFIG_FILE=$(mktemp /tmp/distribution-config.XXXXXX.json)

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query DistributionConfig | jq .Enabled=false >${DISTRIBUTION_CONFIG_FILE}
    aws cloudfront update-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG} --distribution-config file://${DISTRIBUTION_CONFIG_FILE} >/dev/null
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}
}

getETag() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"

    aws cloudfront get-distribution-config --id ${CLOUDFRONT_DISTRIBUTION_ID} --query ETag --output text
}

deleteDistribution() {
    local CLOUDFRONT_DISTRIBUTION_ID="$1"
    local ETAG="$2"

    aws cloudfront delete-distribution --id ${CLOUDFRONT_DISTRIBUTION_ID} --if-match ${ETAG}
}

destroy() {
    local BUCKET_NAME="$1"
    local CLOUDFRONT_DISTRIBUTION_ID="$2"
    local ORIGIN_ACCESS_IDENTITY=$(aws cloudfront get-distribution-config --id "${CLOUDFRONT_DISTRIBUTION_ID}" --query 'DistributionConfig.Origins.Items[0].S3OriginConfig.OriginAccessIdentity' --output text | cut -d '/' -f 3)

    local CACHE_POLICY_ID=$(aws cloudfront get-distribution-config --id "${CLOUDFRONT_DISTRIBUTION_ID}" --query 'DistributionConfig.DefaultCacheBehavior.CachePolicyId' --output text)

    #get etag
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    #disable distribution
    disableDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}
    #get etag
    local ETAG=$(getETag ${CLOUDFRONT_DISTRIBUTION_ID})
    #delete distribution
    deleteDistribution ${CLOUDFRONT_DISTRIBUTION_ID} ${ETAG}

    local CACHE_POLICY_ETAG=$(aws cloudfront get-cache-policy --id ${CACHE_POLICY_ID} --query ETag --output text)
    aws cloudfront delete-cache-policy --id ${CACHE_POLICY_ID} --if-match ${CACHE_POLICY_ETAG}

    local ORIGIN_ACCESS_ID_ETAG=$(aws cloudfront get-cloud-front-origin-access-identity --id "${ORIGIN_ACCESS_IDENTITY}" --query 'ETag' --output text)

    aws cloudfront delete-cloud-front-origin-access-identity --id ${ORIGIN_ACCESS_IDENTITY} --if-match ${ORIGIN_ACCESS_ID_ETAG}

    aws s3 rb --force s3://${BUCKET_NAME}
}

# Pass Bucket Name and Distribution ID
destroy "$2" "$3"
