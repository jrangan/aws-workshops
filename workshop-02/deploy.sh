#!/bin/bash

# How to Run
# ./deploy <absolute path to build folder> <aws sso profile>

set -e
export AWS_PROFILE="$2"

setBucketPolicy() {
    local BUCKET_NAME=$1
    local BUCKET_POLICY_FILE=$(mktemp /tmp/bucket-policy.XXXXXX.json)
    cat <<EOF >${BUCKET_POLICY_FILE}
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "PublicReadGetObject",
            "Effect": "Allow",
            "Principal": "*",
            "Action": [
                "s3:GetObject"
            ],
            "Resource": [
                "arn:aws:s3:::${BUCKET_NAME}/*"
            ]
        }
    ]
}
EOF
    aws s3api put-bucket-policy --bucket ${BUCKET_NAME} --policy file://${BUCKET_POLICY_FILE}
    rm -rf ${BUCKET_POLICY_FILE}
}

deploy() {
    local WEBSITE_CONTENTS="$1"
    local BUCKET_NAME=$(uuidgen)

    # deploy s3 bucket
    aws s3 mb s3://${BUCKET_NAME}

    #enable static webhosting
    aws s3 website s3://${BUCKET_NAME} --index-document index.html

    #remove public block
    aws s3api delete-public-access-block --bucket ${BUCKET_NAME}

    #copy webiste contents into bucket
    aws s3 sync "${WEBSITE_CONTENTS}" s3://${BUCKET_NAME}

    #add bucket policy to allow anyone to get any object in the bucket
    setBucketPolicy ${BUCKET_NAME}

    local S3_HTTP_DOMAIN_NAME="${BUCKET_NAME}.s3-website.ap-southeast-2.amazonaws.com"

    # create cloudfront distribution
    #set default root object to index.html
    local DOMAIN_NAME=$(aws cloudfront create-distribution --origin-domain-name ${S3_HTTP_DOMAIN_NAME} --default-root-object index.html | jq '.Distribution.DomainName' -r)
    local CLOUDFRONT_DISTRIBUTION_ID=$(aws cloudfront list-distributions --profile "$2" --query DistributionList.Items[0].Id --output text)
    aws cloudfront wait distribution-deployed --id ${CLOUDFRONT_DISTRIBUTION_ID}

    # return endpoint
    echo ${DOMAIN_NAME}

}

deploy "$1" "$2"
